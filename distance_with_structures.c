//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct point{
    float x, y;
};
float compute(struct point a, struct point b)
{
    float distance;
    distance = sqrt(((b.x-a.x)*(b.x-a.x))+((b.y-a.y)*(b.y-a.y)));
    return distance;
}
float output(double d)
{
     printf("The distance between point %f\n",d);
}
float main()
{
    struct point a,b;
    float d, out;
    printf("Enter the X-axis points: ");
    scanf("%f%f",&a.x,&b.x);
    printf("Enter Y-axis points: ");
    scanf("%f%f",&a.y,&b.y);
    d = compute(a,b);
    out = output(d);
    
    return 0;
}