#include <stdio.h>
#include<math.h>

struct sumfract{
	int num,den;
};
typedef struct sumfract SumFract;

SumFract input()
{
    SumFract a;
    printf("Enter the numerator\n");
	scanf("%d",&a.num);
	printf("Enter the denominator\n");
	scanf("%d",&a.den);
	return a;
}
SumFract compute(SumFract a, SumFract b)
{
	int i,gcd, numerator, denominator;
	SumFract c;
	numerator = (a.num*b.den)+(b.num*a.den);
	denominator = (a.den*b.den);
	for(i=1;i<=numerator && i<=denominator;++i)
	{
		if(numerator%i==0 && denominator%i==0)
		{
			gcd = i;
		}
	}
	c.num = numerator/gcd;
	c.den = denominator/gcd;
	return c;
}
void output(SumFract c)
{
    printf("The sum of two fractions = %d / %d",c.num,c.den);
}
int main()
{
	SumFract a,b,c;
	printf("Enter the values for Fraction 1\n");
	a=input();
	printf("Enter the values for Fraction 2\n");
	b=input();
	c = compute(a,b);
	output(c);
	return 0;
}