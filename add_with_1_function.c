//Write a program to add two user input numbers using one function.
#include<stdio.h>
float sum( float a, float b)
{
	return (a+b);
}
int main()
{
	float a, b;
	printf("Enter two numbers: ");
	scanf("%f%f",&a,&b);
	printf("Sum of %f and %f = %f\n",a,b,sum(a,b));
	return 0;
}
