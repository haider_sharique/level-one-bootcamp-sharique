//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>

int input()
{
    int n;
    printf ("Enter the total number of digits to add: ");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int i,a[50],sum=0;
    for(i=0;i<n;i++)
    {
        printf("Enter a number\n");
        scanf("%d",&a[i]);
        sum += a[i];
    }
    return sum;
}
int output(int sum)
{
	printf("Total Sum = %d",sum);
}

int main()
{
    int n,x,s;
    n = input();
    x=compute(n);
    s=output(x);
    return 0;
}
